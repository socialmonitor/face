#! /bin/bash

sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get install -y --fix-missing \
build-essential \
cmake \
git \
python3-pip \
libx11-dev \
libblas-dev \
libboost-all-dev

git clone https://github.com/davisking/dlib.git dlib

cd dlib

mkdir build

cd build

sudo /usr/bin/cmake .. -DDLIB_USE_CUDA=0 -DUSE_AVX_INSTRUCTIONS=1

sudo /usr/bin/cmake --build .

cd ..

sudo /usr/bin/python3 setup.py install --no DLIB_USE_CUDA

sudo -H pip3 install --upgrade pip

sudo -H pip3 install pillow matplotlib face_recognition pytest
