from face_recog_utils import Comparator
import os
import numpy as np

def test_face_locations_and_encodings_in_memory():
    comparator = Comparator("known_people_pictures_testing", None)
    (img, locations) = comparator.get_image_and_face_locations(os.path.join("known_people_pictures_testing", "andy.jpg"))

    assert locations == [(262, 187, 316, 133)]

    encodings = comparator.get_face_encodings(img, locations, False)

    assert len(encodings) == 1
    assert len(encodings[0]) == 128

def test_face_encodings_file():
    comparator = Comparator("known_people_pictures_testing", None)
    encodings = comparator.get_face_encodings_file_path(os.path.join("known_people_pictures_testing", "Andy_Zuelsdorf.jpg"), False)

    assert len(encodings) == 1
    assert len(encodings[0]) == 128
