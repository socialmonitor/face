import boto3
import copy
import os

UPLOAD_BUCKET = "andy-libby-face-recognition-pictures"
UPLOAD_DIR = "temp"

class BucketUploader(object):
    def __init__(self, bucket_name, client=None, bucket_acl='private', object_acl='private', bucket_enc=True, object_enc=True, storage_class='STANDARD_IA'):
        if client is None:
            self.client = boto3.client('s3')
        else:
            self.client = client

        self._bucket_name = bucket_name

        self._bucket_acl = bucket_acl
        self._object_acl = object_acl

        self._bucket_enc = bucket_enc
        self._object_enc = object_enc

        self._storage_class = storage_class

        self._all_bucket_names = None

    def get_bucket_names(self, use_cache=True):
        if use_cache and self._all_bucket_names is not None:
            return copy.deepcopy(self._all_bucket_names)

        buckets = list()

        for bucket in self.client.list_buckets().get("Buckets", []):
            bucket_name = bucket.get("Name")

            if bucket_name is not None:
                buckets.append(bucket_name)

        self._all_bucket_names = buckets

        return copy.deepcopy(buckets)

    def safe_create_bucket(self, verbose=False):
        if self._bucket_name not in self.get_bucket_names():
            if verbose:
                print(f"Creating bucket {self._bucket_name} because it does not exist.")

            try:
                self.client.create_bucket(ACL=self._bucket_acl, Bucket=self._bucket_name)

                self.client.put_public_access_block(
                    Bucket=self._bucket_name,
                    PublicAccessBlockConfiguration={
                        'BlockPublicAcls': True,
                        'IgnorePublicAcls': True,
                        'BlockPublicPolicy': True,
                        'RestrictPublicBuckets': True
                    }
                )

                if self._bucket_enc:
                    self.client.put_bucket_encryption(
                        Bucket=self._bucket_name,
                        ServerSideEncryptionConfiguration={
                            'Rules': [
                                {
                                    'ApplyServerSideEncryptionByDefault': {
                                        'SSEAlgorithm': 'AES256'
                                    }
                                }
                            ]
                        }
                    )
                return True
            except BaseException as be:
                if verbose:
                    print(f"Could not create bucket {self._bucket_name} due to: {be}")
                return False

        else:
            if verbose:
                print(f"Not creating bucket {self._bucket_name}. Bucket already exists.")
            return True

    def upload_dir(self, dirname, verbose=False):
        upload_paths = set()

        for entry_path in os.listdir(dirname):
            if os.path.isfile(os.path.join(dirname, entry_path)):
                upload_paths.add(entry_path)

        if not self.safe_create_bucket(verbose=verbose):
            if verbose:
                print(f"Upload from {dirname} failed: Could not create bucket {self._bucket_name}")
                return False

        if verbose:
            print(f"Uploading {', '.join(upload_paths)}")

        upload_number = 1

        for upload_path in upload_paths:
            if verbose:
                print(f"Uploading {upload_path} ({upload_number} of {len(upload_paths)})")

            with open(os.path.join(dirname, upload_path), 'rb') as upload_file:
                if self._object_enc:
                    self.client.put_object(ACL=self._object_acl, Bucket=self._bucket_name, ServerSideEncryption='AES256', Body=upload_file, StorageClass=self._storage_class, Key=upload_path)
                else:
                    self.client.put_object(ACL=self._object_acl, Bucket=self._bucket_name, Body=upload_file, StorageClass=self._storage_class, Key=upload_path)

            if verbose:
                print(f"Uploaded {upload_path}.")

            upload_number += 1

        return True

def main():
    BucketUploader(UPLOAD_BUCKET).upload_dir(UPLOAD_DIR, verbose=True)

if __name__ == "__main__":
    main()
