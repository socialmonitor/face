import os
import matplotlib
import sys
import time
import face_recognition
import cv2
from hashlib import md5
import numpy as np

class Comparator(object):
    # @param encodings_dir str: a string representing the path to a directory
    # where previously computed picture encodings are stored.
    def __init__(self, known_faces_paths, encodings_dir):
        self.known_faces_paths = known_faces_paths

        self.encodings_dir = encodings_dir

        self.detector = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

        self.cached_face_encodings = dict()

    def get_face_locations(self, image):
        grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        rects = self.detector.detectMultiScale(grayscale, scaleFactor=1.5, minNeighbors=5, minSize=(20, 20))

        boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]

        return boxes

    def get_loaded_image(self, image_file_path, verbose=True):
        try:
            return face_recognition.load_image_file(image_file_path)
        except OSError:
            if verbose:
                sys.stderr.write("Could not process image \"{0}\"\n".format(image_file_path))

            return None

    def get_face_encodings_file_path(self, file_path, cache):
        encodings_additional_time = 0

        start_time = time.time()

        face_encodings = None

        if file_path in self.cached_face_encodings:
            face_encodings = self.cached_face_encodings[file_path]
        else:
            encodings_additional_time = time.time() - start_time

            start_time = time.time()
            face = self.get_loaded_image(file_path)
            end_time = time.time()

            start_time = time.time()
            face_encodings = self.get_face_encodings(face, None, cache)

            if cache:
                self.cached_face_encodings[file_path] = face_encodings

        end_time = time.time()

        return face_encodings

    # @description: Retrieves face encodings from an image represented by a numpy
    # array.
    # @param image numpy.array: a numpy array representing an image.
    # @param face_locs list: a list of integer 4-tuples representing a bounding
    # box of coordinates for a face in the image.
    # @return numpy.array[float64]: a 128-element numpy array representing the
    # encodings of different facial features.
    def get_face_encodings(self, image, face_locs, cache):
        # Computing face encodings is an extremely expensive operation, even more
        # expensive than computing face locations, even when the face locations
        # have been previously computed and supplied. To reduce the computational
        # burden of performing these operations, we will save them and reuse them
        # whenever possible. This saving and reusing is detailed in the following
        # comments.

        # If there is no encodings dir specified, then we can't read previous
        # encodings or save newly-computed encodings. So compute the encodings
        # and return them.
        if self.encodings_dir is None:
            return face_recognition.face_encodings(image, known_face_locations=face_locs)

        # If the given image has a previously-computed encoding, we assume it is
        # in the encodings directory with the file name equal to the md5 of
        # the image array's binary string representation.
        encoding_file_path = None
        picture_encoding = None

        if cache:
            encoding_file_name = "{}.jpg".format(md5(image.tostring()).hexdigest())

            encoding_file_path = os.path.join(self.encodings_dir, encoding_file_name)

        # If there is a previously-computed encoding file present, then read the
        # contents, close the file, and return.
        if encoding_file_path is not None and os.path.isfile(encoding_file_path):

            with open(encoding_file_path, 'rb') as encoding_file:
                picture_encoding = np.load(encoding_file, allow_pickle=False)

        # If there is no previously-computed encoding file present, then compute
        # the encodings, serialize them to the properly-named encoding file, and
        # return the results of the encoding.
        else:
            picture_encoding = face_recognition.face_encodings(image, known_face_locations=face_locs)

            if cache:
                with open(encoding_file_path, 'wb') as encoding_file:
                    np.save(encoding_file, picture_encoding, allow_pickle=False)

        return picture_encoding

    def compare_faces_dir(self, image, face_locs):
        start_time = time.time()
        image_face_encodings = self.get_face_encodings(image, face_locs, False)
        end_time = time.time()

        for known_face_file_path in self.known_faces_paths: 
            known_face_encodings = self.get_face_encodings_file_path(known_face_file_path, True)

            start_time = time.time()
            matches = face_recognition.compare_faces(image_face_encodings,
                known_face_encodings[0], tolerance=0.5)
            end_time = time.time()

            found_matches = False

            if matches and len(matches) >= 1:
                for match in matches:
                    if match:
                        print("Known face file {} matches at least one face".format(known_face_file_path))
                        found_matches = True
                        break

            if not found_matches:
                print("Known face file {} matches no faces".format(known_face_file_path))

    def get_image_and_face_locations(self, unknown_file_path):
        start_time = time.time()
        unknown_image = self.get_loaded_image(unknown_file_path)
        end_time = time.time()

        start_time = time.time()
        face_locations = self.get_face_locations(unknown_image)
        end_time = time.time()

        if face_locations is None:
            return (unknown_image, list())

        return (unknown_image, face_locations)
