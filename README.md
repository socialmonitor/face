## Installation

This solution has been developed for Linux.

To install, run `./install.sh` . If you are installing on Raspberry Pi or another resource-constrained system, please ensure you have a minimum of 2GB of RAM. If you do not have this, then please increase your swap space so that your RAM + swap is at least 2GB. The swap space can be decreased again after install.

## Testing

To test, run `./run_tests.sh`
