from picamera import PiCamera
from face_recog_utils import Comparator
import os
import time

def process_picture(comparator, picture_file_path):
    picture, face_locs = comparator.get_image_and_face_locations(picture_file_path)

    if len(face_locs) > 0:
        comparator.compare_faces_dir(picture, face_locs)

def main(): 
    pictures_dir = "pictures"
    known_pictures_dir = "known_people_pictures"
    encodings_dir = "encodings"
    max_pictures = 1000

    camera = PiCamera()

    camera.resolution = (1024, 1024)

    camera.exposure_mode = 'night'

    try:
        if os.path.isdir(known_pictures_dir):
            known_people_locations = [os.path.join(known_pictures_dir, _) for _ in os.listdir(known_pictures_dir)]
        else:
            known_people_locations = []

        if not os.path.isdir(pictures_dir):
            os.makedirs(pictures_dir)

        if not os.path.isdir(encodings_dir):
            os.makedirs(encodings_dir)

        comparator = Comparator(known_people_locations, encodings_dir)

        i = 1

        while True:
            processing_start_time = time.time()
            picture_file_path = os.path.join(pictures_dir, "picture{}.jpg".format(i))

            if i == max_pictures:
                i = 1
            else:
                i += 1

            camera.capture(picture_file_path, format='jpeg', quality=100)

            process_picture(comparator, picture_file_path)

            processing_end_time = time.time()
            print("Finished processing {} . Start time: {} sec after UNIX epoch, end time: {} sec, time elapsed: {} sec".format(picture_file_path, processing_start_time, processing_end_time, processing_end_time - processing_start_time))
    except KeyboardInterrupt:
        print("Encountered keyboard interrupt. Terminating program.")
    finally:
        camera.close()

if __name__ == "__main__":
    main()
